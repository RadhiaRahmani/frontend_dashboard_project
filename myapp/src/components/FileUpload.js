import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
const FileUpload = (props) => {
  // const [file, setFile] = useState("");
  const [file, setfile] = useState("");
  const [languagecontent, setlanguagecontent] = useState([]);
  const saveFile = (e) => {
    setfile(e.target.files[0]);
  };

  const uploadFile = async (e) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("file", file);

    try {
      const res = await axios.post(
        `https://backend-dashboard-hxdw.vercel.app/upload/${props.id}`,
        formData
      );

      console.log(res);
      alert("image ajoutée avec success");
    } catch (ex) {
      console.log(ex);
    }
  };
  // get language content
  const getlanguage = async () => {
    const response = await fetch(
      `https://backend-dashboard-hxdw.vercel.app/language/${props.language}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const result = await response.json();
    setlanguagecontent(result[0]);
    console.log("languagecont" + result);
    // console.log("languagecont" + result);
  };
  // use effect
  useEffect(() => {
    getlanguage();
  }, []);
  return (
    <div>
      <div className="card col-md-4 offset-md-4" style={{ marginTop: "100px" }}>
        <div className="card-header">
          <h4>{languagecontent.text11}</h4>
        </div>
        <div className="card-body">
          <input
            type="file"
            onChange={saveFile}
            name="image"
            accept="image/*"
            multiple={false}
          />
          <button className="btn btn-primary" onClick={uploadFile}>
            {languagecontent.button9}
          </button>
        </div>
      </div>
      {/* <img alt="" src="/imgs/image.jpg"></img> */}
    </div>
  );
};

export default FileUpload;
