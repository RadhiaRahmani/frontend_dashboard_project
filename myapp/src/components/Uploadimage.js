import { useState } from "react";

import { useEffect } from "react";
export default function Uploadimages() {
  const [image, setimage] = useState("");
  const imageHandler = async (event) => {
    event.preventDefault();
    let formData = new FormData();
    const file = event.target.files[0];
    formData.append("image", file);
    const response = await fetch("http://localhost:3001/api/image", {
      method: "POST",
      body: formData,
    });
  };

  const getdata = async () => {
    await fetch("http://localhost:3001/api/image/user/2", {
      method: "GET",
    })
      .then((data) => data.json())
      .then((data) => {
        setimage("http://localhost:3001/public/" + data[0].img_name);
        console.log(image);
      });
  };
  useEffect(() => {
    getdata();
  }, []);
  return (
    <>
      <div className="App">
        <input
          type="file"
          name="image"
          accept="image/*"
          multiple={false}
          onChange={imageHandler}
        />
        {image && <img src={image} alt="img" />}
      </div>
    </>
  );
}
